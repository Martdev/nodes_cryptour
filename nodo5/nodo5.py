import sqlite3
from flask import Flask, request
import requests, rsa, os, uuid, base64, json
from flask_cors import CORS, cross_origin
import warnings
warnings.filterwarnings("ignore")
app = Flask(__name__)
cors = CORS(app)


conn = sqlite3.connect('transacciones.db',  check_same_thread=False)
conn.execute('''
    CREATE TABLE IF NOT EXISTS transacciones (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        origen TEXT,
        destino TEXT,
        monto TEXT,
        hash TEXT
    )
''')

def generateKeysNodo5():
    (publicKey, privateKey) = rsa.newkeys(2048)
    with open('keys/pubkey_nodo5.pem', 'wb') as p:
        p.write(publicKey.save_pkcs1('PEM'))
    with open('keys/privkey_nodo5.pem', 'wb') as p:
        p.write(privateKey.save_pkcs1('PEM'))



if len(os.listdir('/home/ubuntu/nodo/keys')) == 0:
    generateKeysNodo5()
    print("Keys generated!!")
else:    
    print("Keys already generated")

print("Nodo 5 Conectado")
# NODO 4
@app.route('/transaccion', methods=['POST'])
@cross_origin()
def recibir_transaccion_front():

    transaccion = request.json
    nodo = transaccion.get('nodo')
    origen = transaccion.get('origen')
    destino = transaccion.get('destino')
    monto = transaccion.get('monto')
    hash = uuid.uuid4().hex
    encrypted_hash = encrypt_hash(hash,nodo)
    guardar_transaccion(origen, destino, monto, base64.b64encode(encrypted_hash))
    replicar_transaccion(origen, destino, monto, hash)
    return json.dumps({'msg':"Transaccion Exitosa"}), 200, {'ContentType':'application/json'} 


@app.route('/tx', methods=['POST'])
@cross_origin()
def recibir_transaccion_nodo():
    tx = request.json
    origen = tx.get('origen')
    destino = tx.get('destino')
    monto = tx.get('monto')
    encrypted_hash = tx.get('hash')
    if evaluar_integridad(encrypted_hash) == False:
        return json.dumps({'msg':"Error en la integridad de la transaccion"}), 401, {'ContentType':'application/json'} 
    else:
        guardar_transaccion(origen, destino, monto, encrypted_hash)
        print("Transacción recibida en el NODO 5, guardada y replicada correctamente")
        return 'Transacción recibida en el NODO 5, guardada y replicada correctamente'

def guardar_transaccion(origen, destino, monto, hash):
    conn.execute('INSERT INTO transacciones (origen, destino, monto, hash) VALUES (?, ?, ?, ? )', (origen, destino, monto, hash))
    conn.commit()

def encrypt_hash(hash_tx, nodo):
    with open('keys/pubkey_nodo{}.pem'.format(nodo), 'rb') as f:
        public_key = rsa.PublicKey.load_pkcs1(f.read())  
    return rsa.encrypt(hash_tx.encode('ascii'), public_key)

def evaluar_integridad(ciphertext):
    with open('keys/privkey_nodo5.pem', 'rb') as p:
        private_key = rsa.PrivateKey.load_pkcs1(p.read())
    try:
        return rsa.decrypt(base64.b64decode(ciphertext), private_key).decode('ascii')
    except:
        return False

@app.route('/estado', methods=['GET'])
@cross_origin()
def estado_nodo():
    return json.dumps({'Estado':True}), 200, {'ContentType':'application/json'} 


@app.route('/all', methods=['GET'])
@cross_origin()
def get_database():
    ides,origenes,destinos,montos,hashes = [],[],[],[],[]
    cur = conn.cursor()
    cur.execute("SELECT * FROM transacciones")
    rows = cur.fetchall()
    for row in rows:
        ides.append(row[0])
        origenes.append(row[1])
        destinos.append(row[2])
        montos.append(row[3])
        hashes.append(str(row[4]))

    res = {'ides':ides,'origenes':origenes, 'destinos':destinos, 'montos':montos, 'hashes':hashes}
    return json.dumps(res)


def replicar_transaccion(origen, destino, monto, hash):
    # NODO 1
    ip_nodo1 = "https://nodo1.cryptoUR.com/tx"
    hash_tx = base64.b64encode(encrypt_hash(hash,1))
    print("Hash_tx: ",hash_tx)
    print('Transacción replicada al NODO 1: ', ip_nodo1)
    data = {'origen': origen, 'destino': destino, 'monto':monto, 'hash':hash_tx}
    x = requests.post(ip_nodo1, json=data, verify=False)
    print(x.text)
    # NODO 2
    ip_nodo2 = "https://nodo2.cryptoUR.com/tx"
    hash_tx = base64.b64encode(encrypt_hash(hash,2))
    print("Hash_tx: ",hash_tx)
    print('Transacción replicada al NODO 2: ', ip_nodo2)
    data = {'origen': origen, 'destino': destino, 'monto':monto, 'hash':hash_tx}
    x = requests.post(ip_nodo2, json=data, verify=False)
    print(x.text)
    # NODO 3
    ip_nodo3 = "https://nodo3.cryptoUR.com/tx"
    hash_tx = base64.b64encode(encrypt_hash(hash,3))
    print("Hash_tx: ",hash_tx)
    print('Transacción replicada al NODO 3: ', ip_nodo3)
    data = {'origen': origen, 'destino': destino, 'monto':monto, 'hash':hash_tx}
    x = requests.post(ip_nodo3, json=data, verify=False)
    print(x.text)
    # NODO 4
    ip_nodo4 = "https://nodo4.cryptoUR.com/tx"
    hash_tx = base64.b64encode(encrypt_hash(hash,4))
    print("Hash_tx: ",hash_tx)
    print('Transacción replicada al NODO 4: ', ip_nodo4)
    data = {'origen': origen, 'destino': destino, 'monto':monto, 'hash':hash_tx}
    x = requests.post(ip_nodo4, json=data, verify=False)
    print(x.text)



if __name__ == '__main__':
    app.run(host="0.0.0.0")